
from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from twilio.twiml.messaging_response import MessagingResponse

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///C:/Users/teste/Documents/effective-web-scraping/deals.db'
db = SQLAlchemy(app)

class Checkout51Deal(db.Model):
    __tablename__ = 'checkout51'
    id = db.Column(db.Integer, primary_key=True)
    site = db.Column(db.String)
    date = db.Column(db.String)
    title = db.Column(db.String)
    cashback = db.Column(db.String)
    condition = db.Column(db.String)
    link_img = db.Column(db.String)

class CvsDeal(db.Model):
    __tablename__ = 'cvs'
    id = db.Column(db.Integer, primary_key=True)
    site = db.Column(db.String)
    date = db.Column(db.String)
    title = db.Column(db.String)
    price = db.Column(db.String)
    condition = db.Column(db.String)
    link_img = db.Column(db.String)

class HomeDepotDeal(db.Model):
    __tablename__ = 'home_depot'
    id = db.Column(db.Integer, primary_key=True)
    site = db.Column(db.String)
    date = db.Column(db.String)
    title = db.Column(db.String)
    price = db.Column(db.String)
    link_img = db.Column(db.String)

class IbottaDeal(db.Model):
    __tablename__ = 'ibotta'
    id = db.Column(db.Integer, primary_key=True)
    site = db.Column(db.String)
    date = db.Column(db.String)
    title = db.Column(db.String)
    cashback = db.Column(db.String)
    condition = db.Column(db.String)
    link_img = db.Column(db.String)

class LowesDeal(db.Model):
    __tablename__ = 'lowes'
    id = db.Column(db.Integer, primary_key=True)
    site = db.Column(db.String)
    date = db.Column(db.String)
    title = db.Column(db.String)
    price = db.Column(db.String)
    link_img = db.Column(db.String)

class PremiumOutletsDeal(db.Model):
    __tablename__ = 'premium_outlets'
    id = db.Column(db.Integer, primary_key=True)
    site = db.Column(db.String)
    date = db.Column(db.String)
    title = db.Column(db.String)
    price = db.Column(db.String)
    condition = db.Column(db.String)
    expiration_date = db.Column(db.String)
    link_img = db.Column(db.String)

class PublixDeal(db.Model):
    __tablename__ = 'publix'
    id = db.Column(db.Integer, primary_key=True)
    site = db.Column(db.String)
    date = db.Column(db.String)
    title = db.Column(db.String)
    price = db.Column(db.String)
    link_img = db.Column(db.String)
    expiration_date = db.Column(db.String)
    condition = db.Column(db.String)

class TargetDeal(db.Model):
    __tablename__ = 'target'
    id = db.Column(db.Integer, primary_key=True)
    site = db.Column(db.String)
    date = db.Column(db.String)
    title = db.Column(db.String)
    price = db.Column(db.String)
    link_img = db.Column(db.String)

class WalgreensDeal(db.Model):
    __tablename__ = 'walgreens'
    id = db.Column(db.Integer, primary_key=True)
    site = db.Column(db.String)
    date = db.Column(db.String)
    title = db.Column(db.String)
    price = db.Column(db.String)
    link_img = db.Column(db.String)

class WalmartDeal(db.Model):
    __tablename__ = 'walmart'
    id = db.Column(db.Integer, primary_key=True)
    site = db.Column(db.String)
    date = db.Column(db.String)
    title = db.Column(db.String)
    price = db.Column(db.String)
    link_img = db.Column(db.String)

# Mapeamento entre nomes de lojas e modelos de tabelas
modelos_lojas = {
    'target': TargetDeal,
    'walmart': WalmartDeal,
    'home depot': HomeDepotDeal,
    'cvs': CvsDeal,
    'checkout51': Checkout51Deal,
    'lowes': LowesDeal,
    'premium outlets': PremiumOutletsDeal,
    'publix': PublixDeal,
    'walgreens': WalgreensDeal,
    'ibotta': IbottaDeal
}


lojas = list(modelos_lojas.keys())

@app.route('/whatsapp', methods=['POST'])
def whatsapp_webhook():
    incoming_msg = request.form.get('Body', '').lower()
    resp = MessagingResponse()
    msg = resp.message()

    # Função para construir a mensagem de boas-vindas
    def get_welcome_msg():
        lojas_str = '\n'.join([f"- Digite *{loja.title()}* para ofertas da {loja.title()}" for loja in lojas])
        return (
            f"Olá, bem-vindo ao nosso serviço de ofertas! 🛍️\n"
            f"Você pode perguntar sobre ofertas das seguintes lojas:\n{lojas_str}\n"
            "Por favor, digite o nome da loja para ver as ofertas ou 'menu' para ver estas opções novamente."
        )

    if incoming_msg in ['oi', 'olá', 'menu']:
        msg.body(get_welcome_msg())
    elif any(loja in incoming_msg for loja in lojas):
        loja_escolhida = next((loja for loja in lojas if loja in incoming_msg), None)
        if loja_escolhida:
            modelo = modelos_lojas[loja_escolhida]
            deals = modelo.query.limit(5).all()
            deals_msg = '\n'.join([f"{deal.title} - Preço: {deal.price if deal.price else 'Não disponível'}" for deal in deals])
            msg.body(f"As ofertas da {loja_escolhida.title()} são:\n{deals_msg}")
    else:
        msg.body("Desculpe, não entendi a sua solicitação.\n" + get_welcome_msg())

    return str(resp)

if __name__ == '__main__':
    with app.app_context():
        db.create_all()  # Assegure-se de que suas tabelas já existam; isso não irá modificá-las se já estiverem criadas
    app.run(debug=True)